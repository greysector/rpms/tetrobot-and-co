%global _enable_debug_packages %{nil}
%global debug_package          %{nil}
%global __strip       /bin/true
Summary: Puzzle-solving game
Name: tetrobot-and-co
Version: 2.1.0.6
Release: 2
URL: https://www.gog.com/game/tetrobot_and_co
Source0: https://cdn.gog.com/secure/tetrobot_and_co/linux/gog_tetrobot_and_co_2.1.0.6.sh
Source1: %{name}.desktop
License: GOG
BuildArch: i686
BuildRequires: bsdtar
BuildRequires: desktop-file-utils
Requires: libpulse-simple.so.0
Requires: mesa-dri-drivers%{_isa}
Requires: %{_bindir}/lsb_release
Requires: %{_bindir}/pulseaudio

%global __provides_exclude_from ^%{_libdir}/%{name}/Data/Mono/x86/.*\\.so$

%description
Tetrobot is the world's most popular and reliable robot, but he's not perfect.
That's why I, Maya, have built him a microscopic little brother, Psychobot.

Help me guide Psy through Tetrobot's cogs and circuits: by swallowing blocks of
matter and spitting them out, we can repair anything. Learn how pipes, lasers,
fans and other electronic devices work, learn how to alter the chemical
properties of slime, and collect all the memory blocks that you find on the way.

The owners of damaged Tetrobots all around the world rely on us because we are
the only people who can repair their precious robots.

* Pure puzzle gameplay for those who like to give their minds a workout.
* Over 50 levels to play and even more secrets to unlock.
* Original music composed by Morusque (Blocks That Matter, Knytt Stories,
  Saira).
* Find out what became of Tetrobot, Alexey, Markus and Maya, 15 years after
  "Blocks That Matter".

%prep
%setup -qcT
bsdtar -x -f%{S:0}
find "data/noarch/game/Data" -type f -print0 | xargs -0 chmod -x
chmod +x data/noarch/game/Data/Mono/x86/libmono.so

%install
install -dm755 %{buildroot}{%{_bindir},%{_libdir}/%{name}}
cp -pr data/noarch/game/Data %{buildroot}%{_libdir}/%{name}
install -pm755 data/noarch/game/Tetrobot\ and\ Co.x86 %{buildroot}%{_libdir}/%{name}

install -Dpm644 data/noarch/support/icon.png %{buildroot}%{_datadir}/icons/hicolor/256x256/apps/%{name}.png
desktop-file-install --dir %{buildroot}%{_datadir}/applications %{S:1}

%files
%license "data/noarch/docs/End User License Agreement.txt"
%{_datadir}/applications/%{name}.desktop
%{_datadir}/icons/hicolor/256x256/apps/%{name}.png
%{_libdir}/%{name}

%changelog
* Sat Feb 04 2023 Dominik Mierzejewski <rpm@greysector.net> 2.1.0.6-2
- fix startup crash due to missing lsb_release
- fix missing audio due to missing pulseaudio

* Thu Jul 01 2021 Dominik Mierzejewski <rpm@greysector.net> 2.1.0.6-1
- initial build
